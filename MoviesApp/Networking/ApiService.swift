//
//  ApiService.swift
//  MoviesApp
//
//  Created by Ezra Yeoshua on 27/4/21.
//

import Foundation

class ApiService {
    // API key given in email
    var apiKey = "328c283cd27bd1877d9080ccb1604c91"
    private var dataTask: URLSessionDataTask?
    
    // Init pagination
    var page = 1
    var isPaginating = false
    
    // Fetch movie details according to passed movie ID
    func getMovieDetails(movieId: Int, completion: @escaping (Result<MovieDetails, Error>) -> Void) {
        
        // Endpoint
        let movieDetailsURL = "https://api.themoviedb.org/3/movie/" + String(movieId) + "?api_key=" + apiKey + "&language=en-US"
        
        guard let url = URL(string: movieDetailsURL) else {return}
        
        // Create URL session - work on the background
        dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            // Handle error
            if let error = error {
                completion(.failure(error))
                print("DataTask error: \(error.localizedDescription)")
                return
            }
            
            guard let response = response as? HTTPURLResponse else {
                // Handle empty response
                print("Empty response")
                return
            }
            
            print("Response status code: \(response.statusCode)")
            
            guard let data = data else {
                //Handle empty daata
                print("Empty data")
                return
            }
            
            do {
                // Parse data according to MovieDetails model
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(MovieDetails.self, from: data)
                
                DispatchQueue.main.async {
                    completion(.success(jsonData))
                }
                
            } catch let error {
                completion(.failure(error))
            }
        }
        dataTask?.resume()
    }
    
    // Fetch popular movies.
    func getPopularMoviesData(pagination: Bool = false, completion: @escaping (Result<MoviesData, Error>) -> Void) {
        
        if pagination {
            isPaginating = true
        }
        
        // If paginating, fetch next page.
        if isPaginating {
            page+=1
            print(page)
        }
        
        // Endpoint
        let popularMoviesURL = "https://api.themoviedb.org/3/movie/popular?api_key=" + apiKey + "&language=en-US&page="+String(page)
        
        guard let url = URL(string: popularMoviesURL) else {return}
        
        // Create URL session - work on the background
        dataTask = URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            // Handle error
            if let error = error {
                completion(.failure(error))
                print("DataTask error: \(error.localizedDescription)")
                return
            }
            
            guard let response = response as? HTTPURLResponse else {
                // Handle empty response
                print("Empty response")
                return
            }
            
            print("Response status code: \(response.statusCode)")
            
            guard let data = data else {
                //Handle empty daata
                print("Empty data")
                return
            }
            
            do {
                // Parse data according to MoviesData model
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(MoviesData.self, from: data)
                
                DispatchQueue.main.async {
                    completion(.success(jsonData))
                }
                
                // After successful fetch, set pagination back to false.
                if pagination {
                    self.isPaginating = false
                }
                
            } catch let error {
                completion(.failure(error))
            }
        }
        dataTask?.resume()
    }
}
