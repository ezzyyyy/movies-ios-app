//
//  MovieViewModel.swift
//  MoviesApp
//
//  Created by Ezra Yeoshua on 27/4/21.
//

import Foundation

class MovieViewModel {
    private var apiService = ApiService()
    
    private var popularMovies = [Movie]()
    
    // Use API service to fetch popular movies.
    // Save movies into movie array for later.
    // This function is only used for first fetches and refreshes,
    // otherwise, fetchPopularMoviesData() is used.
    func fetchPopularMoviesData(completion: @escaping () -> ()) {
        // Page set back to 1.
        apiService.page = 1
        apiService.getPopularMoviesData(pagination: false){  result in
            switch result {
            case .success(let listOf):
                self.popularMovies = listOf.movies
                completion()
            case .failure(let error):
                print("Error processing json data: \(error)")
            }
        }
    }
    
    // Fetches next pages of popular movies.
    // Sets pagination to true.
    // Appends local array of popular movies.
    func fetchMorePopularMoviesData(completion: @escaping () -> ()) {
        apiService.getPopularMoviesData(pagination: true){ result in
            switch result {
            case .success(let listOf):
                self.popularMovies.append(contentsOf: listOf.movies)
                completion()
            case .failure(let error):
                print("Error processing json data: \(error)")
            }
        }
    }
    
    // Sets number of rows of movie cells according to number of fetched popular movies.
    func numberOfRowsInSection(section: Int) -> Int {
        if popularMovies.count != 0 {
            return popularMovies.count
        }
        return 0
    }
    
    // Sets movie to individual movie cells in list view according to index.
    func cellForRowAt (indexPath: IndexPath) -> Movie {
        return popularMovies[indexPath.row]
    }
}
