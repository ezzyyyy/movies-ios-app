//
//  DetailViewModel.swift
//  MoviesApp
//
//  Created by Ezra Yeoshua on 27/4/21.
//

import Foundation

class DetailViewModel {
    private var apiService = ApiService()
    
    // Fetches movie details according to movie ID passed to API endpoint
    func fetchMovieDetails(movieId: Int,completion: @escaping (MovieDetails) -> Void) {
        apiService.getMovieDetails(movieId: movieId) { result in
            switch result {
            case .success(let data):
                completion(data)
            case .failure(let error):
                print("Error processing json data: \(error)")
            }
        }
    }
}

