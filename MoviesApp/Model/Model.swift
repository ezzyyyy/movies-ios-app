//
//  Model.swift
//  MoviesApp
//
//  Created by Ezra Yeoshua on 27/4/21.
//

import Foundation

// MoviesData struct for collection of Movies
struct MoviesData: Decodable {
    let movies: [Movie]
    
    private enum CodingKeys: String, CodingKey {
        case movies = "results"
    }
}

// Movie struct according to TMDB API
struct Movie: Decodable, Identifiable {
    let id: Int?
    let title: String?
    let year: String?
    let rate: Double?
    let posterImage: String?
    let overview: String?
    
    private enum CodingKeys: String, CodingKey {
        case title, overview, id
        case year = "release_date"
        case rate = "vote_average"
        case posterImage = "poster_path"
    }
    
}

// Movie Details struct according to TMDB API Movie Details
struct MovieDetails: Decodable {
    let id: Int?
    let title: String?
    let year: String?
    let rate: Double?
    let posterImage: String?
    let overview: String?
    let genres: [MovieGenre]?
    let language: String?
    let runtime: Int?
    
    private enum CodingKeys: String, CodingKey {
        case title, overview, id, runtime, genres
        case language = "original_language"
        case year = "release_date"
        case rate = "vote_average"
        case posterImage = "poster_path"
    }
}

// MovieGenre struct according to TMDB API Movie Genre
struct MovieGenre: Decodable {
    let id: Int?
    let name: String?
    
    private enum CodingKeys: String, CodingKey {
        case id, name
    }
}
