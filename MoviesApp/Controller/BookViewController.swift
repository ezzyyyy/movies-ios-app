//
//  BookViewController.swift
//  MoviesApp
//
//  Created by Ezra Yeoshua on 28/4/21.
//

import UIKit

// 3rd party library used (installed using Pods)
import DropDown

class BookViewController: UIViewController {

    @IBOutlet weak var cDropdown: UIView!
    @IBOutlet weak var cDropdownTitle: UILabel!
    @IBOutlet weak var dDropdown: UIView!
    @IBOutlet weak var dDropdownTitle: UILabel!
    @IBOutlet weak var tDropdown: UIView!
    @IBOutlet weak var tDropdownTitle: UILabel!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var bookButton: UIButton!
    
    // Cinemas according to Cathay main page.
    let cinemaArr = ["Cineleisure", "Causeway Point", "The Cathay", "AMK Hub", "Downtown East", "West Mall", "Jem", "Parkway Parade"]
    
    // Fake timings
    let timeArr = ["14:00", "16:45", "19:45"]
    
    let cinemaDropdown = DropDown()
    let datesDropdown = DropDown()
    let timeDropdown = DropDown()
    
    var mTitle = String()
    var selectedTime : String! = nil
    var selectedDate : String! = nil
    var selectedCinema : String! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Create Booking"
        
        movieTitle.text = mTitle
        
        // Set cinema dropdown and display selected value.
        cinemaDropdown.anchorView = cDropdown
        cinemaDropdown.dataSource = cinemaArr
        cinemaDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.cDropdownTitle.text = cinemaArr[index]
            selectedCinema = cinemaArr[index]
        }
        
        
        var datesArr: Array<String> = []
        
        var i = 0
        
        // Create an array of dates starting from today until 11 days from now.
        // Similar to Cathay's dates dropdown.
        while (i <= 11) {
            let d : Date! = Calendar.current.date(byAdding: .day, value: i, to: Date())
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-YYYY"
            datesArr.append(formatter.string(from: d))
            i += 1
        }
        
        // Set dates dropdown and display selected value.
        datesDropdown.anchorView = dDropdown
        datesDropdown.dataSource = datesArr
        datesDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.dDropdownTitle.text = datesArr[index]
            selectedDate = datesArr[index]
        }
        
        // Set timing dropdown and display selected value.
        timeDropdown.anchorView = tDropdown
        timeDropdown.dataSource = timeArr
        timeDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.tDropdownTitle.text = timeArr[index]
            selectedTime = timeArr[index]
        }
        
        bookButton.addTarget(self, action: #selector(confirmButtonClicked), for: .touchUpInside)
    }
    
    // Display an alert upon clicking Confirm to show user's final selections.
    // If one field at all is empty, user is prompted to fill the rest of the fields.
    @objc func confirmButtonClicked(_ sender: UIButton) {
        if ((selectedTime == nil) || (selectedDate == nil) || (selectedCinema == nil)) {
            let alert = UIAlertController(title: "Oops!", message: "Please fill all the following fields to book a movie.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                    case .default:
                    print("default")
                    
                    case .cancel:
                    print("cancel")
                    
                    case .destructive:
                    print("destructive")
                    
                @unknown default:
                    fatalError()
                }
            }))
            self.present(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertController(
                title: "Booking Confirmation",
                message: "Movie: "+mTitle+"\n Cinema: "+selectedCinema+"\n Date: "+selectedDate+"\n Time: "+selectedTime,
                preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                switch action.style{
                    case .default:
                    print("default")
                    
                    case .cancel:
                    print("cancel")
                    
                    case .destructive:
                    print("destructive")
                    
                @unknown default:
                    fatalError()
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    // Display cinema dropdown options
    @IBAction func showCinemaOptions(_ sender: Any) {
        cinemaDropdown.show()
    }
    
    // Display date dropdown options
    @IBAction func showDatesOptions(_ sender: Any) {
        datesDropdown.show()
    }
    
    // Display timing dropdown options 
    @IBAction func showTimeOptions(_ sender: Any) {
        timeDropdown.show()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
