//
//  ViewController.swift
//  MoviesApp
//
//  Created by Ezra Yeoshua on 27/4/21.
//

import UIKit

class MovieViewController: UITableViewController {
    private var viewModel = MovieViewModel()
    private var detailsViewModel = DetailViewModel()
    private var movieId = Int()
    var apiService = ApiService()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Popular Movies"
        
        loadPopularMoviesData()
        
        // Init pull to refresh feature
        let refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        
        // Set refresh feature to our UITableView
        tableView.refreshControl = refreshControl
        tableView.delegate = self
    }
    
    // Fetch and load popular movies upon refresh.
    @objc func refresh(_ sender: AnyObject) {
        loadPopularMoviesData()
        sender.endRefreshing()
    }
    
    // Set UITableView data to our fetched popular movies
    private func loadPopularMoviesData() {
        viewModel.fetchPopularMoviesData { [weak self] in
            self?.tableView.dataSource = self
            self?.tableView.reloadData()
        }
    }
    
    // Set UITableView data to our fetched popular movies
    private func loadMorePopularMoviesData() {
        viewModel.fetchMorePopularMoviesData { [weak self] in
            self?.tableView.dataSource = self
            self?.tableView.reloadData()
        }
    }
    
    // Get respective movie ID according to tapped movie cell.
    // Navigate to Details View of selected movie ID (using segue).
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MovieTableViewCell
        
        let movie = viewModel.cellForRowAt(indexPath: indexPath)
        let movieId = cell.getMovieId(movie)
        print(movieId ?? "no movie Id")
        self.movieId = movieId ?? 0
        
        performSegue(withIdentifier: "detailViewSegue", sender: cell)
    }
    
    // If navigating to Details view, pass the movie ID returned over to DetailsViewController.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailViewSegue" {
            let destinationController = segue.destination as! DetailViewController
            destinationController.movieId = self.movieId
        }
    }
    
    // Number of rows returned to UITableView.
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return viewModel.numberOfRowsInSection(section: section)
    }
    
    // Set our UITableView cell values according to movie instance returned.
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->
    UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MovieTableViewCell
        
        let movie = viewModel.cellForRowAt(indexPath: indexPath)
        cell.setCellWithValuesOf(movie)
        
        return cell
    }
    
    // If the scroll reaches the bottom of the View, fetch more popular movies data and append to list.
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let position = scrollView.contentOffset.y
        if position > ((tableView.contentSize.height-100)-(scrollView.frame.size.height)) {
            guard !apiService.isPaginating else {
                return
            }
            loadMorePopularMoviesData()
        }
    }
}
