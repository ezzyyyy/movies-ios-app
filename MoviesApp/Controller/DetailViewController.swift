//
//  DetailViewController.swift
//  MoviesApp
//
//  Created by Ezra Yeoshua on 27/4/21.
//

import UIKit

class DetailViewController: UIViewController {
    private var detailViewModel = DetailViewModel()
    var movieId = Int()
    var mTitle = String()
    private var urlString: String = ""
    
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var overview: UILabel!
    @IBOutlet weak var genre: UILabel!
    @IBOutlet weak var runtime: UILabel!
    @IBOutlet weak var language: UILabel!
    @IBOutlet weak var bookButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Movie Details"
        
        movieTitle.text = String(movieId)
        
        // Fetch movie details according to movie ID and set the values returned according to their respective UI components.
        detailViewModel.fetchMovieDetails(movieId: movieId) { (result) -> Void in
            let title : String! = result.title
            let overview : String! = result.overview
            let runtime : String! = String(result.runtime ?? 0)
            let language : String! = result.language
            let poster : String! = result.posterImage
            let genres : Array<MovieGenre> = result.genres ?? []
            self.movieTitle.text = title
            self.overview.text = overview
            self.runtime.text = runtime+"mins"
            self.language.text = language
            
            var genre = String()
            for (g) in genres {
                if (g.name != genres[genres.count-1].name) {
                    genre += (String(g.name ?? "") + ", ")
                } else {
                    genre += String(g.name ?? "")
                }
            }
            
            self.genre.text = genre
            
            self.movieTitle.numberOfLines = 0
            self.movieTitle.lineBreakMode = .byWordWrapping
            self.movieTitle.sizeToFit()
            self.overview.numberOfLines = 0
            self.overview.lineBreakMode = .byWordWrapping
            self.overview.sizeToFit()
            
            
            guard let posterString = poster else {return}
            self.urlString = "https://image.tmdb.org/t/p/w300"+posterString
            guard let posterImageURL = URL(string: self.urlString) else {
                self.posterImage.image = UIImage(named: "noImageAvailable")
                return
            }
            self.posterImage.image = nil
            self.getImageDataFrom(url: posterImageURL)
            
            self.mTitle = title
        }
        
        bookButton.addTarget(self, action: #selector(bookButtonClicked), for: .touchUpInside)
    }
    
    // Upon clicking Book button, navigate to Booking page.
    @objc func bookButtonClicked(_ sender: UIButton) {
        performSegue(withIdentifier: "bookViewSegue", sender: Any?.self)
    }
    
    // If navigating to Booking page, send the movie title over to BookViewController.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "bookViewSegue" {
            let destinationController = segue.destination as! BookViewController
            destinationController.mTitle = self.mTitle
        }
    }
    
    // Get poster image data
    private func getImageDataFrom(url: URL) {
        URLSession.shared.dataTask(with: url) { (data,response,error) in
            // Handle error
            
            if let error = error {
                print("datatask error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data else {
                print("Empty data")
                return
            }
            
            DispatchQueue.main.async {
                if let image = UIImage(data:data) {
                    self.posterImage.image = image
                }
            }
        }.resume()
    }
}
